<?php

//Set options for sideGalleri
$arrConfig = array(
    'galleryId' => $modx->getOption('galleryId', $scriptProperties, 5),
    'itemClass' => $modx->getOption('itemClass', $scriptProperties, 'galItem'),
    'wrapperClass' => $modx->getOption('wrapperClass', $scriptProperties, 'galWrap'),
    'innerDropdownClass' => $modx->getOption('innerDropdownClass', $scriptProperties, 'dropdown-menu'),
    'parentDropdownClass' => $modx->getOption('parentDropdownClass', $scriptProperties, 'dropdown')
);

if(!function_exists('view_album')) {
    function view_album($modx, $arrConfig, $data, $iteration) {
        $arr = "";
        $iteration = isset($iteration) ? $iteration : 0;
        ++$iteration;
        
        foreach ($data as $menu) {
            if(!empty($menu['childs'])) {
                $arr .= '<li class="' . $arrConfig['itemClass'] . ' ' . $arrConfig['parentDropdownClass'] .  $menu['galleri-active'] . '"><a href="' . $modx->makeUrl($arrConfig['galleryId']) . '?galAlbum=' . $menu['id'] . '">' . $menu['name'] . '</a>';
                $arr .= view_album($modx, $arrConfig, $menu['childs'], $iteration) . '</li>';
            }else{
                $arr .= '<li class="' . $arrConfig['itemClass'] . $menu['galleri-active'] . '"><a href="' . $modx->makeUrl($arrConfig['galleryId']) . '?galAlbum=' . $menu['id'] . '">' . $menu['name'] . '</a></li>';            
            }
        }
        if($iteration > 1) {
            return '<ul class="' . $arrConfig['innerDropdownClass'] . '">' . $arr . '</ul>';
        }else{
            return '<ul class="' . $arrConfig['wrapperClass'] . '">' . $arr . '</ul>';
        }
    }
}

if(!function_exists("map_tree")) {
    function map_tree($row) {
        $tree = array();
        $currAlbum = $_GET['galAlbum'];
        
        foreach ($row as $k => &$v) {
            if (!$v['parent']) {
                $tree[$k] = &$v;
            }else{
                $row[$v['parent']]['childs'][$k] = &$v;
            }
        }
        return $tree;
    }
}
if(!function_exists("checkActive")) {
    function checkActive(&$row, $currAlbum) {
        if(!empty($currAlbum)) {
            foreach($row as &$item) {
                if($item['id'] == $currAlbum) {
                    $item['galleri-active'] = ' active';
                    
                    if($item['parent']) {
                        checkActive($row, $item['parent']);
                    }
                }
            }
        }
    }
}

$stmt = $modx->query("SELECT * FROM modx_gallery_albums WHERE active = 1 AND prominent = 1");
$row = $stmt->fetchAll(PDO::FETCH_ASSOC);
$data = array();
foreach($row as $k => $v) {
    $data[$v['id']] = $v;
}

$currAlbum = $_GET['galAlbum'];
checkActive($data, $currAlbum);
$allMenu = map_tree($data);

return view_album($modx, $arrConfig, $allMenu, $iteration);